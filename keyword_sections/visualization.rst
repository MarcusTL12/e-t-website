visualization
-------------

The ``visualization`` section is used for plotting orbitals and densities.

Keywords
^^^^^^^^
.. container:: sphinx-custom

   ``file format: [string]``

   Default: ``plt``

   Specify format of the output files containing the grid data. Optional.

   Valid options are:

   - ``plt``
   - ``cube``


.. container:: sphinx-custom

   ``grid buffer: [real]``

   Default: :math:`2.0` (or 2.0d0)

   Sets the distance between the edge of the grid (x, y, and z direction)
   and the molecule in Angstrom units. Optional.


.. container:: sphinx-custom

   ``grid max: {[real], [real], [real]}``

   Sets the maximum values of the grid in x, y and z direction in Angstrom. Optional.

   .. note::
      ``grid min`` is required if ``grid max`` is specified.

      ``grid buffer`` is not used in this case.


.. container:: sphinx-custom

   ``grid min: {[real], [real], [real]}``

   Sets the minimum values of the grid in x, y and z direction in Angstrom. Optional.

   .. note::
      ``grid max`` is required if ``grid min`` is specified.

      ``grid buffer`` is not used in this case.


.. container:: sphinx-custom

   ``grid spacing: [real]``

   Default: :math:`0.1` (or 1.0d-1)

   Sets the spacing between grid points for the visualization given in Angstrom units. Optional.


.. container:: sphinx-custom

   ``plot cc density``

   Plots the coupled cluster density. Optional.

   .. note::
      This keyword is only read if ``cc mean value`` or ``response`` is specified in the ``do`` section.


.. container:: sphinx-custom

   ``plot es densities``

   Plots the coupled cluster excited state densities for the ``states to plot``. Optional.

   .. note::
      This keyword is only read if ``response`` is specified in the ``do`` section.


.. container:: sphinx-custom

   ``plot hf active density``

   Plots the active HF density in the case of a reduced space calculation. Optional.

   .. note::
      This keyword is only read if ``hf`` is specified in the ``frozen orbitals`` section.


.. container:: sphinx-custom

   ``plot hf density``

   Plots the HF density. Optional.


.. container:: sphinx-custom

   ``plot hf orbitals: {[integer], [integer], ...}``

   Plots the canonical orbitals given in the comma separated list. Optional.


.. container:: sphinx-custom

   ``plot transition densities``

   Plots the coupled cluster transition densities for the ``states to plot``. Optional.

   .. note::
      This keyword is only read if ``response`` is specified in the ``do`` section.


.. container:: sphinx-custom

   ``states to plot: {[integer], [integer], ...}``

   List of integers specifying which densities should be plotted.

   .. note::
      By default the densities of the states specified in ``initial states``
      from the response section are plotted.


.. container:: sphinx-custom

   ``plot cc orbitals: {[integer], [integer], ...}``

   Plots the orbitals given in the comma separated list from a CC wave function. Optional.

   .. note::
      This keyword is used to visualize the orbitals in ``MLCC`` or after freezing of orbitals.
