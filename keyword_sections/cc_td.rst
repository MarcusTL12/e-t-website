
cc td
-----

Keywords related to time-dependent coupled cluster calculations go into the ``cc td`` section.

.. note:: 
   
   One of the keywords below must be specified if you have requested ``time dependent state`` in the ``do`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``propagation``
   
   Default: ``false``
   
   Perform real-time propagation of the coupled-cluster state. 

.. container:: sphinx-custom

   ``fft dipole moment``
   
   Default: ``false``
   
   Perform complex fast Fourier transform of the dipole moment time series from an earlier real-time propagation calculation.

.. container:: sphinx-custom

   ``fft electric field``
   
   Default: ``false``
   
   Perform complex fast Fourier transform of the electric field time series from an earlier real-time propagation calculation.

