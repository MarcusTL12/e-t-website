frozen orbitals
---------------

The ``frozen orbitals`` section is used to freeze orbitals for a reduced space coupled cluster calculation. 

Orbitals which can be frozen:

- Core orbitals, for the frozen core approximation
- Hartree-Fock orbitals in the case where an active space is defined for the coupled cluster calculation.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``hf``

   Default: false

   This enables the freezing of Hartree-Fock orbitals after they are converged. Optional.

   .. note::
      This keyword depends on a coupled cluster ``active atoms`` space being defined in the :ref:`active atoms section<active-atoms-section>`.

.. container:: sphinx-custom

   ``core``

   Default: false

   This enables the frozen core approximation. Optional.

   .. warning::
      This keyword can currently not be used if core excited states are to be calculated.
