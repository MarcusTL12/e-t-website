
integrals
---------

In the integrals section you can specify settings for the handling of Cholesky vectors and electron repulsion integrals in coupled cluster calculations.

Keywords
^^^^^^^^^

.. container:: sphinx-custom
   
   ``cholesky storage: [string]``

   Default: ``memory`` if they take up less than 20% of the total memory; ``disk`` otherwise

   Specify how to store the Cholesky vectors. Optional.

   Valid options are:

   - ``memory`` Store Cholesky vectors in memory. 
   - ``disk`` Store Cholesky vectors on file. 

.. container:: sphinx-custom
   
   ``eri storage: [string]``

   Default: ``memory`` if the entire matrix is needed in the calculation and they take up less than 20% of the total memory; ``none`` otherwise

   Specify how to store the electron repulsion integrals. Optional.

   Valid options are:

   - ``memory`` Store full electron repulsion matrix in memory. 
   - ``none`` Always construct the integrals from Cholesky vectors.

   .. warning::

      ``memory`` can slow down calculations for which the full integral matrix is not needed. This is the case for CCS and CC2 calculations. We recommend to use defaults. 

.. container:: sphinx-custom
   
   ``mo eri in memory``
   
   Default: ``false``
   
   Override defaults and store full MO ERI matrix in memory if true.
   Recommended for time-dependent CC.
   
.. container:: sphinx-custom

   ``t1 eri in memory``
   
   Default: ``false``
   
   Override defaults and store full T1 ERI matrix in memory if true.

.. container:: sphinx-custom

   ``ri: [string]``

   Default: ``None``

   Enables the RI approximation for the integrals and provides the auxiliary basis set.

   Valid options are any available auxiliary basis set.
