solver tdhf es
--------------

Keywords related to solving the TDHF equations go into the ``solver tdhf es`` section.
Required to find TDHF excitation energies. Currently TDHF is only implemented for RHF.

Required keywords
^^^^^^^^^^^^^^^^^
.. container:: sphinx-custom

   ``singlet states: [integer]``

   Specifies the number of singlet excited states to calculate.


Optional keywords
^^^^^^^^^^^^^^^^^
.. container:: sphinx-custom

   ``energy threshold: [real]``

   Default: :math:`10^{-3}` (or 1.0d-3)

   Energy convergence threshold, as measured with respect to the previous iteration. Optional.

   .. note::
      The solvers will not check for the energy convergence, if the energy threshold is not set.

.. container:: sphinx-custom

   ``max iterations: [integer]``

   Default: 100

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number.
   Optional.

.. container:: sphinx-custom

   ``max reduced dimension: [integer]``

   Default: 50

   The maximal dimension of the reduced space of the Davidson procedure. Optional.


.. container:: sphinx-custom

   ``residual threshold: [real]``

   Default: :math:`10^{-3}` (or 1.0d-3)

   Threshold of the :math:`L^{2}`-norm of the residual vector of the excited state equation. Optional.


.. container:: sphinx-custom

   ``restart``

   Default: false

   If specified, the solver will attempt to restart from a previous calculation. Optional.

.. container:: sphinx-custom

   ``storage: [string]``

   Default: ``memory``

   Selects storage of excited state records. Optional.

   Valid keyword values are:

   - ``memory`` Stores records in memory.
   - ``disk`` Stores records on file.

.. container:: sphinx-custom

   ``tamm-dancoff``

   Default: false

   Enables the Tamm-Dancoff approximation. Optional.

